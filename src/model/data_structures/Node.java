package model.data_structures;

public class Node<T> {
	private T element;
	private Node<T> next;
	private Node<T> previous;
	
	public Node(T element, Node<T> pAnterior, Node<T> pSiguiente){
		this.element = element;
		next = pSiguiente;
		previous = pAnterior;
	}
	
	public void cambiarSiguiente(Node<T> x){
		next = x;
	}
	
	public Node<T> darSiguiente(){
		return next;
	}
	
	public void cambiarAnterior(Node<T> x){
		previous = x;
	}
	
	public Node<T> darAnterior(){
		return previous;
	}
	
	public T getElement(){
		return element;
	}
}
