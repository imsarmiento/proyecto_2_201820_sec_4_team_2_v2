package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Queue<T> implements ICola<T>{

	private Node<T> first;
	private Node<T> last;
	private int size;
	@Override
	public Iterator<T> iterator() {
		return new ListIterator<T>(first);
	}


	public Queue() {
		// TODO Auto-generated constructor stub
		first = null;
		last = null;
		size = 0;
	}
	@Override
	public boolean isEmpty() {
		return size==0;
	}

	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return size;
	}



	@Override
	public T dequeue() {
		if(isEmpty()) throw new NoSuchElementException ("No hay elementos para sacar");
		Node<T> oldFirst = first;
		Node<T> newFirst = first.darSiguiente();
		first = newFirst;
		size--;
		if(isEmpty()) last = null;
		return oldFirst.getElement();
	}


	@Override
	public void enqueue(T t) {
		// TODO Auto-generated method stub
		 Node<T> agregado = new Node<T>(t,null,null);
		 if(isEmpty())
		 {
			 first = agregado;
			 last = agregado;
			 size++;
		 }
		 else
		 {
			 last.cambiarSiguiente(agregado);
			 last = agregado;
			 size++;
		 }
	}


}
