package model.data_structures;

import java.util.Iterator;

public class ArbolRojoNegro<Key extends Comparable<Key>, Value> {
	private static final boolean RED   = true;
	private static final boolean BLACK = false;

	private NodeTree<Key, Value> root;


	public ArbolRojoNegro(){
		root = null;
	}
	public NodeTree<Key,Value> darRaiz(){
		return root;
	}
	public void put(Key key, Value val) {
		if (key == null) 
			throw new IllegalArgumentException("first argument to put() is null");
		if (val == null) {
			delete(key);
			return;
		}
		root = put(root, key, val);
		root.cambiarAColor(BLACK);
	}

	private NodeTree<Key, Value> put(NodeTree<Key, Value> h, Key key, Value val) { 

		//Si pasa este if es porque llego a una hoja. Crea el nodo
		if (h == null) return new NodeTree<Key, Value>(key, val, RED, 1);

		int indiceComparador = key.compareTo((Key) h.darLlave());
		
		if      (indiceComparador < 0) h.asignarHijoIzq(put(h.darHijoIzq(),  key, val)); 
		else if (indiceComparador > 0) h.asignarHijoDer(put(h.darHijoDer(), key, val)); 
		else              h.asignarValor(val);
		
		// Arreglar desbalances
		if (isRed(h.darHijoDer()) && !isRed(h.darHijoIzq()))      h = rotateLeft(h);
	    if (isRed(h.darHijoIzq())  &&  isRed(h.darHijoIzq().darHijoIzq())) h = rotateRight(h);
	    if (isRed(h.darHijoIzq())  &&  isRed(h.darHijoDer()))     flipColors(h);
		h.cambiarTamaño(size(h.darHijoIzq()) + size(h.darHijoDer()) + 1);
		return h;
	}

	private boolean isRed(NodeTree<Key,Value> nodo){
		if (nodo == null)  return false;
		return nodo.esRojo() == RED;
	}
	private NodeTree<Key, Value> delete(NodeTree<Key, Value> h, Key key) { 

		if (key.compareTo((Key) h.darLlave()) < 0)  {
			if (h.darHijoIzq().esRojo() && !h.darHijoIzq().darHijoIzq().esRojo())
				h = moveRedLeft(h);
			h.asignarHijoIzq(delete(h.darHijoIzq(), key));
		}
		else {
			if (h.darHijoIzq().esRojo())
				h = rotateRight(h);
			if (key.compareTo((Key) h.darLlave()) == 0 && (h.darHijoDer() == null))
				return null;
			if (!h.darHijoDer().esRojo() && !h.darHijoDer().darHijoIzq().esRojo())
				h = moveRedRight(h);
			if (key.compareTo((Key) h.darLlave()) == 0) {
				NodeTree<Key, Value> x = min(h.darHijoDer());
				h.asignarLlave(x.darLlave());
				h.asignarValor(x.darValor());
				h.asignarHijoDer(deleteMin(h.darHijoDer()));
			}
			else h.asignarHijoDer(delete(h.darHijoDer(), key));
		}
		return balance(h);
	}private NodeTree<Key, Value> balance(NodeTree<Key, Value> h) {
		// assert (h != null);

		if (h.darHijoDer().esRojo())  h = rotateLeft(h);
		if (h.darHijoIzq().esRojo() && h.darHijoIzq().darHijoIzq().esRojo()) h = rotateRight(h);
		if (h.darHijoIzq().esRojo() && h.darHijoDer().esRojo())     flipColors(h);

		h.cambiarTamaño(size(h.darHijoIzq()) + size(h.darHijoDer()) + 1);
		return h;
	}

	//asume que h es rojo y 2 a su izquierda rojos tambien
	private NodeTree<Key, Value> moveRedLeft(NodeTree<Key, Value> h) {
		flipColors(h);
		if (h.darHijoDer().darHijoIzq().esRojo()) { 
			h.asignarHijoDer(rotateRight(h.darHijoDer()));
			h = rotateLeft(h);
			flipColors(h);
		}
		return h;	
	}

	private NodeTree<Key, Value> moveRedRight(NodeTree<Key, Value> h) {
		flipColors(h);
		if (h.darHijoIzq().darHijoIzq().esRojo()) { 
			h = rotateRight(h);
			flipColors(h);
		}
		return h;
	}

	private NodeTree<Key, Value> deleteMin(NodeTree<Key, Value> h) { 
		if (h.darHijoIzq() == null)
			return null;

		if (!h.darHijoIzq().esRojo() && !h.darHijoIzq().darHijoIzq().esRojo())
			h = moveRedLeft(h);

		h.asignarHijoIzq(deleteMin(h.darHijoIzq()));
		return balance(h);
	}

	public void delete(Key key) { 
		if (key == null) throw new IllegalArgumentException("argument to delete() is null");
		if (!contains(key)) return;

		// Si ambos hijos son negros, volver la raiz roja
		if (!root.darHijoIzq().esRojo() && !root.darHijoIzq().esRojo())
			root.cambiarAColor(RED);

		root = delete(root, key);
		if (!isEmpty()) root.cambiarAColor(BLACK);
	}

	private NodeTree<Key, Value> rotateRight(NodeTree<Key, Value> h) {
		NodeTree<Key, Value> x = h.darHijoIzq();
		h.asignarHijoIzq(x.darHijoDer());
		x.asignarHijoDer(h);
		x.cambiarAColor(x.darHijoDer().esRojo());
		x.darHijoDer().cambiarAColor(RED);
		x.cambiarTamaño(h.darTamaño());
		h.cambiarTamaño(size(h.darHijoIzq()) + size(h.darHijoDer()) + 1);
		return x;
	}

	private NodeTree <Key, Value>rotateLeft(NodeTree<Key, Value> h) {
		NodeTree<Key, Value> x = h.darHijoDer();
		h.asignarHijoDer(x.darHijoIzq());
		x.asignarHijoIzq(h);
		x.cambiarAColor(x.darHijoIzq().esRojo());
		x.darHijoIzq().cambiarAColor(RED);
		x.cambiarTamaño(h.darTamaño());
		h.cambiarTamaño(size(h.darHijoIzq()) + size(h.darHijoDer()) + 1);
		return x;
	}
	private void flipColors(NodeTree<Key, Value> h) {
		h.cambiarAColor(!h.esRojo());
		h.darHijoIzq().cambiarAColor(!h.darHijoIzq().esRojo());
		h.darHijoDer().cambiarAColor(!h.darHijoDer().esRojo());
	}

	public int size (){
		return size(root);
	}
	public int size(NodeTree<Key, Value> x) {
		if (x == null) return 0;
		return x.darTamaño();
	} 

	public Value get(Key key) {
		if (key == null) throw new IllegalArgumentException("Argumento es null");
		return get(root, key);
	}

	private Value get(NodeTree x, Key key) {
		while (x != null) {
			int indiceComparador = key.compareTo((Key) x.darLlave());
			if      (indiceComparador < 0) x = x.darHijoIzq();
			else if (indiceComparador > 0) x = x.darHijoDer();
			else              return (Value) x.darValor();
		}
		return null;
	}

	private NodeTree<Key,Value> getNodeTree(NodeTree<Key,Value> x, Key key) {
		while (x != null) {
			int indiceComparador = key.compareTo((Key) x.darLlave());
			if      (indiceComparador < 0) x = x.darHijoIzq();
			else if (indiceComparador > 0) x = x.darHijoDer();
			else              return x;
		}
		return null;
	}
	
	public boolean contains(Key key) {
		return get(key) != null;
	}


	public boolean isEmpty() {
		return root == null;
	}
	public int height() {
        return height(root);
    }
    private int height(NodeTree<Key,Value> x) {
        if (x == null) return -1;
        return 1 + Math.max(height(x.darHijoIzq()), height(x.darHijoDer()));
    }
    
	public int getHeight(Key key){
		if (key == null || !contains(key))
				return -1;
		return getHeight(key, root);
	}
	private int getHeight(Key key, NodeTree<Key, Value> node) {
		if (node.darLlave().equals(key))
			return -1;
		else{
			if (node.darLlave().compareTo(key) > 0){
				return 1 + getHeight(key, node.darHijoDer());
			}else{
				return 1 + getHeight(key, node.darHijoIzq());
			}
		}
	}
	private NodeTree<Key, Value> min(NodeTree<Key, Value> x) { 
		if (x.darHijoIzq() == null) return x; 
		else          
			return min(x.darHijoIzq()); 
	} 
	
    public Key min() throws Exception {
        if (isEmpty()) throw new Exception("El simbolo es vacío");
        return min(root).darLlave();
    } 

    public Key max() throws Exception {
        if (isEmpty()) throw new Exception("El símbolo es vacío");
        return max(root).darLlave();
    } 

    private NodeTree<Key, Value> max(NodeTree<Key, Value> x) { 
        if (x.darHijoDer() == null) return x; 
        else                 
        	return max(x.darHijoDer()); 
    } 
    
    public boolean check(){
    	return isBST() && verificarHijosDeNodoRojo(root);
    }
    private boolean isBST() {
        return isBST(root, null, null);
    }

    private boolean isBST(NodeTree<Key,Value> x, Key min, Key max) {
        if (x == null) return true;
        if (min != null && x.darLlave().compareTo(min) <= 0) return false;
        if (max != null && x.darLlave().compareTo(max) >= 0) return false;
        return isBST(x.darHijoIzq(), min, x.darLlave()) && isBST(x.darHijoDer(), x.darLlave(), max);
    } 
    
    private boolean verificarHijosDeNodoRojo( NodeTree<Key, Value> node ) 
	{
		if( node == null )
			return true;
		else if( node.esRojo() == true)
		{
			if( node.darHijoDer() != null && node.darHijoDer().esRojo() == true ){
				return false;
			}
			if( node.darHijoIzq() != null && node.darHijoIzq().esRojo() == true ){
				return false;
			}
		}

		return verificarHijosDeNodoRojo( node.darHijoDer() ) && verificarHijosDeNodoRojo( node.darHijoIzq() );
	}


	private boolean verificarOrden( NodeTree<Integer, Integer> node ) throws Exception
	{
		if( node == null || (node.darHijoDer() == null && node.darHijoIzq() == null) )
		{
			return true;
		}
		else if( node.darHijoDer() != null )
		{
			if( node.darLlave().compareTo( (Integer) node.darHijoDer().darLlave() ) > 0 )
				
				throw new Exception( "El orden de los nodos no es correcto 1" );
			else if( !verificarOrden( node.darHijoDer() ) )
				throw new Exception( "El orden de los nodos no es correcto 2" );
		}
		else if( node.darHijoIzq() != null )
		{
			if( node.darLlave().compareTo( (Integer) node.darHijoIzq().darLlave()) < 0 )
				throw new Exception( "El orden de los nodos no es correcto 3" );
			else if( !verificarOrden( node.darHijoIzq() ) )
				throw new Exception( "El orden de los nodos no es correcto 4" );
		}
		return true;
	}
	
	public Iterator<Key> keys() {
        if (isEmpty()) return new Queue<Key>().iterator();
        try {
			return keys(min(), max());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }

    public Iterator<Key> keys(Key lo, Key hi) {
        if (lo == null) throw new IllegalArgumentException("first argument to keys() is null");
        if (hi == null) throw new IllegalArgumentException("second argument to keys() is null");

        Queue<Key> queue = new Queue<Key>();
        keys(root, queue, lo, hi);
        return queue.iterator();
    } 
    private void keys(NodeTree<Key,Value> x, Queue<Key> queue, Key lo, Key hi) { 
        if (x == null) return; 
        int cmplo = lo.compareTo((Key) x.darLlave()); 
        int cmphi = hi.compareTo((Key) x.darLlave()); 
        if (cmplo < 0) keys(x.darHijoIzq(), queue, lo, hi); 
        if (cmplo <= 0 && cmphi >= 0) queue.enqueue((Key) x.darLlave()); 
        if (cmphi > 0) keys(x.darHijoDer(), queue, lo, hi); 
    }
    
    public Iterator<Value> valuesInRange(Key init, Key end){
    	Iterator<Key> llaves = keys(init, end);
    	Queue<Value> valores = new Queue<Value>();
    	while(llaves.hasNext()){
    		Key x = llaves.next();
    		valores.enqueue(getNodeTree(root, x).darValor());
    	}
    	return valores.iterator();
    }
    
    public Iterator<Key> keysInRange(Key init, Key end){
    	return keys(init, end);
    }
}