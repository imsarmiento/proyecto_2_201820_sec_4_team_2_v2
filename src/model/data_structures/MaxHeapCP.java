package model.data_structures;
public class MaxHeapCP<T extends Comparable<T>> {

	private T[] priorityQueue;
	private int n;
	private int maxCapacidad;

	/**
	 * Crea un objeto de la clase con una capacidad máxima de elementos pero vacía
	 */
	public MaxHeapCP(int max)
	{

		maxCapacidad = max+1;
		priorityQueue = (T[]) new Comparable[max+1];
		n = 0;
	}

	/**
	 * Retorna número de elementos presentes en la cola de prioridad
	 */
	public int darNumElementos()
	{
		return n;
	}

	/**
	 * Agrega un elemento a la cola. 
	 * Se genera Exception en el caso que se sobrepase el tamaño máximo de la cola
	 */
	public void agregar(T elemento) throws Exception
	{
		if (n == maxCapacidad) throw new Exception("Se alcanzo el tamaño máximo de la cola");
		else 
		{
			priorityQueue[++n] = elemento;
			swim(n);
		}
	}

	/**
	 * Saca/atiende el elemento máximo en la cola y lo retorna; null en caso de cola vacía 
	 */
	public T max()
	{
		if (esVacia()) return null;
		T max = priorityQueue[1];
		exch(1, n--);
		sink(1);
		priorityQueue[n+1] = null;  
		return max;		
	}
	/**
	 * Permite consultar el elemento más grande.
	 */
	public T maxPeak() {
		if (esVacia()) return null;
		return priorityQueue[1];
	}


	/**
	 * Ubica el elemento en la posicion deseada. Se usa cuando tengo un elemento muy grande que
	 * esta muy abajo.
	 */
	private void swim(int k) {
		while (k > 1 && less(k/2, k)) {
			exch(k, k/2);
			k = k/2;
		}
	}
	/**
	 * Ubica el elemento en la posicion deseada. Se usa cuando tengo un elemento pequeño, que esta 
	 * muy arriba
	 */
	private void sink(int k) { 
		while (2*k <= n) {
			int j = 2*k;
			if (j < n && less(j, j+1)) j++;
			if (!less(k, j)) break;
			exch(k, j);
			k = j;
		}
	} 



	/**
	 * Indica si el elemento en la posicióin i es menor que el elemento en la posicion j
	 */
	private boolean less(int i, int j) {
		return ((Comparable<T>) priorityQueue[i]).compareTo(priorityQueue[j]) < 0;
	}

	/**
	 * Intercambia el elemento en la posicion i con el de la posicon j
	 */
	private void exch(int i, int j) {
		T temp = priorityQueue[i];
		priorityQueue[i] = priorityQueue[j];
		priorityQueue[j] = temp;
	}




	/**
	 * Retorna si la cola está vacía o no
	 */
	public boolean esVacia()
	{
		return n == 0;
	}

	/**
	 * Retorna la capacidad máxima de la cola
	 */
	public int tamanoMax()
	{
		return maxCapacidad;

	}

	/**
	 * Empieza la recursion para saber si el Heap esta ordenado. Empieza en 1
	 */
	public boolean estaHeap() {
		return estaHeap(1);
	}

	/**
	 * Sigue la recursion para saber si el Heap esta ordenado. Termina cuando la posición k, ya no
	 * está dentro del Heap
	 */
	public boolean estaHeap(int k) {
		if (k > n) return true;
		int left = 2*k;
		int right = 2*k + 1;
		if (left  <= n && less(k, left))  return false;
		if (right <= n && less(k, right)) return false;
		return estaHeap(left) && estaHeap(right);
	}

}