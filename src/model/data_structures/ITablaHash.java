package model.data_structures;

import java.util.Iterator;

public interface ITablaHash <V, K extends Comparable<K>> extends Iterable<K> {
	
	public void put(K llave, V valor);
	
	public V get(K llave);
	
	public V delete(K llave);
	
	public Iterable<K> keys();

}
