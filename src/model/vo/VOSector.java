package model.vo;

import model.data_structures.ILista;
import model.data_structures.List;

public class VOSector implements Comparable<VOSector> {
	
	private int id;
	private double maxLong;
	private double minLong;
	private double maxLat;
	private double minLat;
	private List<BikeRoute> rutasQueEmpiezanAca;
	private List<Station> estacionesQueEstanAca;
	
	public VOSector(int id, double maxLong, double minLong, double maxLat, double minLat) {
		this.id = id;
		this.maxLong = maxLong;
		this.minLong = minLong;
		this.maxLat = maxLat;
		this.minLat = minLat;
		rutasQueEmpiezanAca = new List<>();
		estacionesQueEstanAca = new List<>();

	}

	
	public void agregarRuta(BikeRoute ruta)
	{
		rutasQueEmpiezanAca.add(ruta);
	}

	/**
	 * @return the rutasQueEmpiezanAca
	 */
	public List<BikeRoute> getRutasQueEmpiezanAca() {
		return rutasQueEmpiezanAca;
	}


	/**
	 * @param rutasQueEmpiezanAca the rutasQueEmpiezanAca to set
	 */
	public void setRutasQueEmpiezanAca(List<BikeRoute> rutasQueEmpiezanAca) {
		this.rutasQueEmpiezanAca = rutasQueEmpiezanAca;
	}


	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**
	 * @return the maxLong
	 */
	public double getMaxLong() {
		return maxLong;
	}


	/**
	 * @param maxLong the maxLong to set
	 */
	public void setMaxLong(double maxLong) {
		this.maxLong = maxLong;
	}


	/**
	 * @return the minLong
	 */
	public double getMinLong() {
		return minLong;
	}


	/**
	 * @param minLong the minLong to set
	 */
	public void setMinLong(double minLong) {
		this.minLong = minLong;
	}


	/**
	 * @return the maxLat
	 */
	public double getMaxLat() {
		return maxLat;
	}


	/**
	 * @param maxLat the maxLat to set
	 */
	public void setMaxLat(double maxLat) {
		this.maxLat = maxLat;
	}


	/**
	 * @return the minLat
	 */
	public double getMinLat() {
		return minLat;
	}


	/**
	 * @param minLat the minLat to set
	 */
	public void setMinLat(double minLat) {
		this.minLat = minLat;
	}


	@Override
	public int compareTo(VOSector o) {
		// TODO Auto-generated method stub
		return 0;
	}


	public void agregarEstacion(Station stat) {
		// TODO Auto-generated method stub
		estacionesQueEstanAca.add(stat);
	}

	public ILista<Station> getEstacionesQueEstanAca() {
		// TODO Auto-generated method stub
		return estacionesQueEstanAca;
	}

}
