package model.vo; 

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RouteGSON implements Comparable<RouteGSON> {

@SerializedName("sid")
@Expose
private String sid;
@SerializedName("id")
@Expose
private String id;
@SerializedName("position")
@Expose
private Integer position;
@SerializedName("created_at")
@Expose
private Integer createdAt;
@SerializedName("created_meta")
@Expose
private Object createdMeta;
@SerializedName("updated_at")
@Expose
private Integer updatedAt;
@SerializedName("updated_meta")
@Expose
private Object updatedMeta;
@SerializedName("meta")
@Expose
private String meta;
@SerializedName("BIKEROUTE")
@Expose
private String bIKEROUTE;
@SerializedName("the_geom")
@Expose
private String theGeom;
@SerializedName("TYPE")
@Expose
private String tYPE;
@SerializedName("STREET")
@Expose
private String sTREET;
@SerializedName("F_STREET")
@Expose
private String fSTREET;
@SerializedName("T_STREET")
@Expose
private String tSTREET;
@SerializedName("STATUS")
@Expose
private String sTATUS;
@SerializedName("Shape_Leng")
@Expose
private String shapeLeng;

public String getSid() {
return sid;
}

public void setSid(String sid) {
this.sid = sid;
}

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public Integer getPosition() {
return position;
}

public void setPosition(Integer position) {
this.position = position;
}

public Integer getCreatedAt() {
return createdAt;
}

public void setCreatedAt(Integer createdAt) {
this.createdAt = createdAt;
}

public Object getCreatedMeta() {
return createdMeta;
}

public void setCreatedMeta(Object createdMeta) {
this.createdMeta = createdMeta;
}

public Integer getUpdatedAt() {
return updatedAt;
}

public void setUpdatedAt(Integer updatedAt) {
this.updatedAt = updatedAt;
}

public Object getUpdatedMeta() {
return updatedMeta;
}

public void setUpdatedMeta(Object updatedMeta) {
this.updatedMeta = updatedMeta;
}

public String getMeta() {
return meta;
}

public void setMeta(String meta) {
this.meta = meta;
}

public String getBIKEROUTE() {
return bIKEROUTE;
}

public void setBIKEROUTE(String bIKEROUTE) {
this.bIKEROUTE = bIKEROUTE;
}

public String getTheGeom() {
return theGeom;
}

public void setTheGeom(String theGeom) {
this.theGeom = theGeom;
}

public String getTYPE() {
return tYPE;
}

public void setTYPE(String tYPE) {
this.tYPE = tYPE;
}

public String getSTREET() {
return sTREET;
}

public void setSTREET(String sTREET) {
this.sTREET = sTREET;
}

public String getFSTREET() {
return fSTREET;
}

public void setFSTREET(String fSTREET) {
this.fSTREET = fSTREET;
}

public String getTSTREET() {
return tSTREET;
}

public void setTSTREET(String tSTREET) {
this.tSTREET = tSTREET;
}

public String getSTATUS() {
return sTATUS;
}

public void setSTATUS(String sTATUS) {
this.sTATUS = sTATUS;
}

public String getShapeLeng() {
return shapeLeng;
}

public void setShapeLeng(String shapeLeng) {
this.shapeLeng = shapeLeng;
}

@Override
public int compareTo(RouteGSON o) {
	// TODO Auto-generated method stub
	return 0;
}

}