package model.vo;

import model.vo.Station;
import model.vo.Trip;

public class ViajeDistancia implements Comparable<ViajeDistancia> {
	
	public final static int EARTH_RADIOUS = 6731000;

	private Trip viaje;
	private Station estacion1;
	private Station estacion2;
	private double distancia;
	
	public ViajeDistancia(Trip viaje, Station estacion1, Station estacion2) {
		this.viaje = viaje;
		this.estacion1 = estacion1;
		this.estacion2 = estacion2;
		calcularDistancia();
	}

	
	public Trip getViaje() {
		return viaje;
	}


	public void setViaje(Trip viaje) {
		this.viaje = viaje;
	}


	public void calcularDistancia()
	{
		double lat1 = Math.toRadians(estacion1.getLatitude());
		double lat2 = Math.toRadians(estacion2.getLatitude());
		double difLat = Math.toRadians(estacion2.getLatitude()-estacion1.getLatitude());
		double difLon = Math.toRadians(estacion2.getLongitude()-estacion1.getLongitude());
		double a = Math.sin(difLat/2)*Math.sin(difLat/2) + Math.cos(lat1)*Math.cos(lat2)*Math.sin(difLon/2)*Math.sin(difLon/2);
		double c = 2*Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		distancia = EARTH_RADIOUS * c;
	}
	
	


	public double getDistancia() {
		return distancia;
	}


	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}


	@Override
	public int compareTo(ViajeDistancia o) {
		if(distancia>o.distancia)
			return 1;
		else if(distancia<o.distancia)
			return -1;
		else
			return 0; 
	}
}