package model.vo;

import java.time.LocalDateTime;
import java.time.Month;

public class Trip implements Comparable<Trip> {

	public final static String MALE = "male";
	public final static String FEMALE = "female";
	public final static String UNKNOWN = "unknown";

	public final static int START_TIME = 1;
	public final static int BIKE_ID = 2;
	public final static int STOP_TIME = 3;
	public final static int DURATION = 4;	

	private int tripId;
	private LocalDateTime startTime;
	private LocalDateTime stopTime;

	private int bikeId;
	private int tripDuration;
	private int startStationId;
	private int endStationId;
	private String gender;
	private boolean deInicio;
	private boolean isSubscriber;
	
	private String startStationName;
	private String endStationName;
	
	private double distancia;

	public Trip(int tripId, String startTime, String stopTime, int bikeId, int tripDuration, int startStationId, int endStationId, String gender, boolean isSubscriber) 
	{
		this.tripId = tripId;
		/**
		DateTimeFormatter formatter1  = null;
		DateTimeFormatter formatter2  = null;
		if(startTime.length()==16)
			formatter1 = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm");
		else if (startTime.length()==13)
			formatter1 = DateTimeFormatter.ofPattern("M/d/yyyy H:mm");
		else 
			formatter1 = DateTimeFormatter.ofPattern("MM/d/yyyy H:mm");
		if(stopTime.length()==16)
			formatter2 = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm");
		else if (stopTime.length()==13)
			formatter2 = DateTimeFormatter.ofPattern("M/d/yyyy H:mm");
		else 
			formatter2 = DateTimeFormatter.ofPattern("MM/d/yyyy H:mm");

		this.startTime = LocalDateTime.parse(startTime, formatter1);
		this.stopTime = LocalDateTime.parse(stopTime, formatter2);
		 */

		String fecha = startTime.substring(0, startTime.indexOf(" "));
		String[] fechaAr = fecha.split("/");
		String hora = startTime.substring(startTime.indexOf(" ")).trim();
		String[] horaAr = hora.split(":");
		this.startTime = LocalDateTime.of(Integer.valueOf(fechaAr[2]), Month.of(Integer.valueOf(fechaAr[0])),Integer.valueOf(fechaAr[1]),Integer.valueOf(horaAr[0]),Integer.valueOf(horaAr[1]));

		fecha = stopTime.substring(0, stopTime.indexOf(" "));
		fechaAr = fecha.split("/");
		hora = stopTime.substring(stopTime.indexOf(" ")).trim();
		horaAr = hora.split(":");
		this.stopTime = LocalDateTime.of(Integer.valueOf(fechaAr[2]), Month.of(Integer.valueOf(fechaAr[0])),Integer.valueOf(fechaAr[1]),Integer.valueOf(horaAr[0]),Integer.valueOf(horaAr[1]));

		this.bikeId = bikeId;
		this.tripDuration = tripDuration;
		this.startStationId = startStationId;
		this.endStationId = endStationId;
		this.gender = gender;
		this.deInicio = false;
		this.isSubscriber = isSubscriber;
		distancia = 0.0;
	}

	public String getStartStationName() {
		return startStationName;
	}

	public void setStartStationName(String startStationName) {
		this.startStationName = startStationName;
	}

	public String getEndStationName() {
		return endStationName;
	}

	public void setEndStationName(String endStationName) {
		this.endStationName = endStationName;
	}

	@Override
	public int compareTo(Trip o) {
		return compareTo(o,1);
	}

	public void setDistancia(double nueva)
	{
		distancia = nueva;
	}
	
	public double getDistancia()
	{
		return distancia;
	}
	public int compareTo(Trip o, int caso) {
		switch (caso)
		{
		case START_TIME: 
			return startTime.compareTo(o.startTime);
		case BIKE_ID:
			if(bikeId>o.bikeId)
				return 1;
			else if(bikeId<o.bikeId)
				return -1;
			else
				return 0;
			
		case DURATION:
			if(tripDuration>o.tripDuration)
				return 1;
			else if(tripDuration<o.tripDuration)
				return -1;
			else
				return 0;
		case STOP_TIME:
			return stopTime.compareTo(o.stopTime);

		}
		return 0;		
	}

	public int getTripId() {
		return tripId;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public LocalDateTime getStopTime() {
		return stopTime;
	}

	public int getBikeId() {
		return bikeId;
	}

	public int getTripDuration() {
		return tripDuration;
	}

	public int getStartStationId() {
		return startStationId;
	}

	public int getEndStationId() {
		return endStationId;
	}

	public String getGender() {
		return gender;
	}
	public void cambiarADeInicio(){
		deInicio=true;
	}
	public boolean getInicio(){
		return deInicio;
	}
	public boolean isSubscriber(){
		return isSubscriber;
	}
	public void asignarEstacionesIncioFin(String inicio, String pFinal){
		startStationName = inicio;
		endStationName = pFinal;
	}
}
