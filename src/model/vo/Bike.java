package model.vo;

import java.util.Iterator;

public class Bike implements Comparable<Bike>{

	private int bikeId;
	private int totalTrips;
	private int totalDistance;
	private int totalDuration;

	public final static int TOTAL_TRIPS = 1;
	public final static int DISTANCE = 2;
	public final static int TOTAL_DURATION = 3;
	
	public Bike(int bikeId, int totalTrips, int totalDistance, int ptotalDuration) {
		this.bikeId = bikeId;
		this.totalTrips = totalTrips;
		this.totalDistance = totalDistance;
		this.totalDuration = ptotalDuration;
	}

	@Override
	public int compareTo(Bike o) {
		return compareTo(o, 1);
	}

	public int compareTo(Bike o, int caso) {
		switch (caso)
		{
		case TOTAL_TRIPS:
			if(totalTrips>o.totalTrips)
				return 1;
			else if(totalTrips<o.totalTrips)
				return -1;
			else
				return 0;
		case DISTANCE:
			if(totalDistance>o.totalDistance)
				return -1;
			else if(totalDistance<o.totalDistance)
				return 1;
			else
				return 0;
		case TOTAL_DURATION:
			if(totalDuration>o.totalDuration)
				return -1;
			else if(totalDuration<o.totalDuration)
				return 1;
			else
				return 0;
		}
		return 0;	   
	}

	public int getBikeId() {
		return bikeId;
	}

	public int getTotalTrips() {
		return totalTrips;
	}

	public int getTotalDistance() {
		return totalDistance;
	}
	public int getTotalDuration() {
		return totalDuration;
	}
}
