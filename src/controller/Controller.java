package controller;

import model.data_structures.ICola;

import model.data_structures.ILista;
import model.logic.Manager;
import model.vo.Bike;
import model.vo.BikeRoute;
import model.vo.ResultadoCampanna;
import model.vo.Station;
import model.vo.Trip;

import java.time.LocalDate;
import java.time.LocalDateTime;

import API.IManager;


public class Controller {
    private static IManager manager = new Manager();

    public static void cargarDatos(String dataTrips, String dataStations, String dataBikeRoutes){
    	manager.cargarDatos(dataTrips, dataStations, dataBikeRoutes);
    }
    
    public static ICola<Trip> A1(int n, LocalDate fechaTerminacion) {
        return manager.A1(n, fechaTerminacion);
    }

    public static ILista<Trip> A2(int n){
        return manager.A2(n);
    }

    public ILista<Trip> A3(int n, LocalDate fecha) {
        return manager.A3(n, fecha);
    }

    public ILista<Bike> B1(int limiteInferior, int limiteSuperior) {
        return manager.B1(limiteInferior, limiteSuperior);
    }

    public ILista<Trip> B2(String estacionDeInicio, String estacionDeLlegada, int limiteInferiorTiempo, int limiteSuperiorTiempo) {
        return manager.B2(estacionDeInicio, estacionDeLlegada, limiteInferiorTiempo, limiteSuperiorTiempo);
    }

    public int[] B3(String estacionDeInicio, String estacionDeLlegada) {
		return manager.B3(estacionDeInicio, estacionDeLlegada);
	}

    public static ResultadoCampanna C1(double valorPorPunto, int numEstacionesConPublicidad, int mesCampanna) {
		return manager.C1(valorPorPunto, numEstacionesConPublicidad, mesCampanna);
	}
    
    public double[] C2(int LA, int LO) {
		return manager.C2(LA, LO);
	}
    

	public int darSector(double latitud, double longitud) {
		return manager.darSector(latitud, longitud);
	}

	public ILista<Station> C3(double latitud, double longitud) {
		return manager.C3(latitud, longitud);
	}

	public ILista<BikeRoute> C4(double latitud, double longitud) throws Exception {
		return manager.C4(latitud, longitud);
	}

	public ILista<BikeRoute> C5(double latitudI, double longitudI, double latitudF, double longitudF) throws Exception {
		return manager.C5(latitudI, longitudI, latitudF, longitudF);
	}

	


}
