package test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.Queue;

/**
 * Clase de prueba para la Cola
 */
public class QueueTest {

	private Queue<Integer> colaPrueba;
	
	private void setupEscenario1( )
	{
		colaPrueba = new Queue<>();
	}
	
	@Test
	public void test() {
		setupEscenario1();
		for(int i = 0; i<10;i++)
		{
			colaPrueba.enqueue(i);
		}
		assertEquals( "La cantidad de elementos deberia ser 10", 10, colaPrueba.getSize());
		assertTrue( "El primer numero en salir debería ser 0",colaPrueba.dequeue()==0);
		for(int i = 1; i<10;i++)
		{
			colaPrueba.dequeue();
		}
		assertTrue("La lista deberia estar vacia", colaPrueba.isEmpty());
	}
}
