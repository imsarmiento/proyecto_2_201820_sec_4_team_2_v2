package test;

import static org.junit.Assert.*;



import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.*;

public class ListTest {

	private List<Integer> prueba;
	private List<Double> prueba2;

	
	@Before
	public void setupEscenario1( )
	{
		prueba = new List<>();
	}
	
	public void setupEscenario2( )
	{
		prueba2 = new List<Double>();
	}
	
	@Test
	public void testAddFirst() 
	{
		setupEscenario1();
		assertNull("La lista está vacía, deberia retornar null", prueba.getHead());
		for (int i = 0; i<10; i++)
		{
			prueba.add(i+1);
		}
		assertTrue("La cabeza deberia ser 0", prueba.getHead().getElement()==1);
		assertTrue("Deberia ser 1", prueba.get(2)==3);
		assertTrue("Deberia ser 2", prueba.get(3)==4);
		assertTrue("Deberia ser 3", prueba.get(4)==5);
		assertTrue("Deberia ser 4", prueba.get(5)==6);
		assertTrue("La cola deberia ser 9", prueba.get(9)==10);
		Node<Integer> act = prueba.getHead();
		for (int i = 0; i<10; i++)
		{
			System.out.println(act.getElement());
			act = act.darSiguiente();
		} 
	}
	
	
	
	
}
